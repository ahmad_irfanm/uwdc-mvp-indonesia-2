<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $hidden  = [];

    // Set Date Response
    public function getDateAttribute($value) {
        return date('d.m.Y', strtotime($value));
    }

    // Category Relationships
    public function category() {
        return $this->belongsTo(Category::class);
    }

    // Type Relationships
    public function type() {
        return $this->belongsTo(Type::class);
    }

    // Tags Relationships
    public function tags() {
        return $this->belongsToMany(Tag::class, 'training_tags');
    }
}
