export default [
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import('@/views/training-sessions/TrainingIndex.vue'),
  },
    {
        path: '/training/new',
        name: 'training-add',
        component: () => import('@/views/training-sessions/TrainingForm.vue'),
    },
    {
        path: '/training/:id/edit',
        name: 'training-edit',
        component: () => import('@/views/training-sessions/TrainingForm.vue'),
    },
]
